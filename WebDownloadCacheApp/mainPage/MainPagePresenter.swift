//
//  MainPagePresenter.swift
//  WebDownloadCacheApp
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit
import WebDownloadCache

protocol mainPageViewDelegate : class{
    func reloadCollectionView()
    func updateCellforIndexPath(indexPath:IndexPath)
}

class MainPagePresenter: NSObject {
    
    let mainPageURL = "http://pastebin.com/raw/wgkJgazE"
    weak var delegate :mainPageViewDelegate?
    var pinterestList :Pinterest?
    
    init(viewdelegate delegate_:mainPageViewDelegate) {
        delegate = delegate_
    }
    
    deinit {
        
    }
    
    func prepareListToShow()
    {
        weak var weakSelf = self
        WebDownloadCacheSDK.getSharedSDK().loadJSONData(with: URL.init(string: mainPageURL)) { (jsonarray,jsonData, error) in
            do
            {
                if(error == nil )
                {
                    self.pinterestList = try JSONDecoder().decode(Pinterest.self, from:jsonData!)
                    if(weakSelf?.delegate != nil)
                    {
                        weakSelf?.delegate?.reloadCollectionView()
                    }
                }
            }
            catch{
                
            }
            
        }
    }
    
    func updateCellContent(cell:gridViewCellCollectionViewCell,for indexPath:IndexPath)
    {
        if let pinterest = pinterestList
        {
            let element = pinterest[indexPath.row]
            cell.updateCellContent(element: element)
        }
    }
    
    func getListCount() -> Int
    {
        return pinterestList?.count ?? 0
    }
    
    
    
}
