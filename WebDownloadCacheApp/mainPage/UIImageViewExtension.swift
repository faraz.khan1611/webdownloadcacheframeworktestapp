//
//  UIImageView.swift
//  WebDownloadCacheApp
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit
import WebDownloadCache

extension UIImageView {
    
    func loadImageFromWeb(url:String?)
    {
        if let url_ = url
        {
            weak var weakSelf = self
            WebDownloadCacheSDK.getSharedSDK().loadImage(with: URL.init(string: url_), fromCache: true) { (image, error) in
                if(error == nil && image != nil)
                {
                    DispatchQueue.main.async {
                        weakSelf?.image = image
                    }
                }
                
            }
        }
    }
}
