//
//  gridViewCellCollectionViewCell.swift
//  WebDownloadCacheApp
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

class gridViewCellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var thumbnailImage: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func updateCellContent(element:PinterestElement)
    {
        self.artistNameLabel.text = element.user.name
        self.thumbnailImage.loadImageFromWeb(url: element.urls.thumb)
    }
}
