//
//  ViewController.swift
//  WebDownloadCacheApp
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit
class ViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,mainPageViewDelegate {
    
    let gridViewCellIdentifier = "gridViewCell"
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    var presenter:MainPagePresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib.init(nibName: "gridViewCellCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: gridViewCellIdentifier)
        presenter = MainPagePresenter(viewdelegate: self)
        loader.startAnimating()
        presenter?.prepareListToShow()
        // Do any additional setup after loading the view, typically from a nib.
    }

    //Collection View Delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: gridViewCellIdentifier, for: indexPath) as! gridViewCellCollectionViewCell
        cell.contentView.layer.cornerRadius = 10.0
        cell.contentView.layer.borderWidth = 2.0
        cell.contentView.layer.borderColor = UIColor.white.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        cell.layer.shadowRadius = 0.0
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        presenter.updateCellContent(cell: cell,for: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.getListCount() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width/2 - 5.0, height: 150);
    }
    
    func reloadCollectionView() {
        DispatchQueue.main.async {
            self.loader.stopAnimating()
            self.collectionView.reloadData()
        }
    }
    
    func updateCellforIndexPath(indexPath: IndexPath) {
        
    }

}

